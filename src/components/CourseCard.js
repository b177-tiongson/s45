import {useState} from 'react'
import PropTypes from 'prop-types'
import { Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function CourseCard({courseProp}) {
	// console.log(props)
	// console.log(typeof props)

    const{_id ,name,description,price} = courseProp

    //Use the state hook for this component to be able to store its state
    //State are used to keep track of information related to individual components
    //Syntax:
        // const [getter,setter] = useState(initialGetterValue)


    // const [count, setCount] = useState(0)
    // console.log(useState(0))

    //function that keeps track of enrollees for a course
    // function enroll(){
    //     setCount(count + 1)
    //     console.log('Enrolees '+ count)
    // }

    // const [getSeat, setSeat] = useState(30)

    // function seat(){
    //     setSeat(getSeat -1)

    //     if(getSeat<=0){
    //         alert("No more seats available")
    //     }
    //     else{
    //         enroll()
    //     }

    // }

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}

//check if the CourseCard component is getting the correct prop types
CourseCard.propTypes = {
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}
